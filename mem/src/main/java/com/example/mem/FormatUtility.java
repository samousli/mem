package com.example.mem;


import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Formats numeric values into a proper string representation
 */
public class FormatUtility {

    /**
     * formats a number in kilobytes into a number in megabytes with 2 decimal points.
     *
     * @param num number in kilobytes
     * @return the string representation
     */
    public static String memFormat(int num) {
        return BigDecimal.valueOf(num / 1024F).setScale(2, RoundingMode.HALF_EVEN).toString() + "MB";
    }

    /**
     * formats a number in bytes into a number in megabytes with 2 decimal points.
     * @param num number in bytes
     * @return the string representation
     */
    public static String storageFormat(float num) {
        return BigDecimal.valueOf(num / (float) 1048576).setScale(2, RoundingMode.HALF_EVEN).toString() + "MB";
    }
}
