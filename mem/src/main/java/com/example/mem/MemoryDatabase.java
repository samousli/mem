package com.example.mem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Database connection
 */
public class MemoryDatabase extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "MemoryDB";


    // Table name
    private static final String TABLE = "MemoryData";

    // Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_INTERNAL = "internalMemory";
    private static final String KEY_EXTERNAL = "externalMemory";
    private static final String KEY_DATE = "date";

    private static final String[] COLUMNS = {KEY_ID, KEY_INTERNAL, KEY_EXTERNAL, KEY_DATE};


    public MemoryDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table
        String CREATE_MEMORY_TABLE = "CREATE TABLE MemoryData  ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "date TEXT, " +
                "internalMemory TEXT, " +
                "externalMemory TEXT )";

        // create memory table
        db.execSQL(CREATE_MEMORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older memory table if existed
        db.execSQL("DROP TABLE IF EXISTS Memory");

        // create new memory table
        this.onCreate(db);
    }

    public void addDayData(DayData data) {
        //Log.d("Add data", data.toString());

        // get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, data.getDate());
        values.put(KEY_INTERNAL, data.getInternalMemory());
        values.put(KEY_EXTERNAL, data.getExternalMemory());

        // insert
        db.insert(TABLE, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // close
        db.close();
    }

    public List<DayData> getAllData() {
        List<DayData> allData = new LinkedList<DayData>();

        // build the query
        String query = "SELECT " + KEY_DATE + ", " +
                KEY_INTERNAL + ", " + KEY_EXTERNAL + " FROM " + TABLE;

        // get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // go over each row, build data and add it to list
        DayData data = null;
        if (cursor.moveToFirst()) {
            do {
                data = new DayData(false);
                data.setNewData(cursor.getString(0), cursor.getString(1), cursor.getString(2));
                allData.add(data);
            } while (cursor.moveToNext());
        }

        //Log.d("getAllData()", allData.toString());
        // return data
        return allData;
    }
}
