package com.example.mem;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.List;

public class UsageDataFragment extends Fragment {
    private MemoryDatabase MemoryDB = null;
    private View view;
    private List<DayData> allData;
    private ListView listView;
    private UsageDataAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.usage_data_tab, container, false);

        this.listView = (ListView) view.findViewById(R.id.usageDataList);

        File database = getActivity().getDatabasePath("MemoryDB.db");

        if (!database.exists()) {
            // Database does not exist so create it
            MemoryDB = new MemoryDatabase(getActivity());
        }

        allData = MemoryDB.getAllData();

        this.addTodaysData();

        adapter = new UsageDataAdapter(getActivity(), R.layout.usage_data_view_item, allData);
        listView.setAdapter(adapter);

        return view;
    }


    public void addTodaysData() {
        DayData today = new DayData(true);
        // If the database is empty or the last entry has a different date than today
        // ignore the request to add data
        if (allData.isEmpty()
                || today.getDate().compareTo(allData.get(allData.size() - 1).getDate()) != 0) {
            MemoryDB.addDayData(today);
        }
    }

    class UsageDataAdapter extends BaseAdapter {
        Context context;
        List<DayData> data;
        int layoutId;

        public UsageDataAdapter(Context context, int layoutId, List<DayData> data) {
            this.context = context;
            this.layoutId = layoutId;
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View row = view;
            UsageInfoHolder holder = null;
            DayData item = data.get(i);

            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(this.layoutId, viewGroup, false);

                holder = new UsageInfoHolder();
                holder.date = (TextView) row.findViewById(R.id.usage_data_date);
                holder.internal = (TextView) row.findViewById(R.id.usage_data_internal);
                holder.external = (TextView) row.findViewById(R.id.usage_data_external);
                row.setTag(holder);
            } else {
                holder = (UsageInfoHolder) row.getTag();
            }

            holder.date.setText(item.getDate());
            holder.internal.setText(item.getInternalMemory());
            holder.external.setText(item.getExternalMemory());

            return row;
        }
    }

    private static class UsageInfoHolder {
        TextView date;
        TextView internal;
        TextView external;
    }
}
