package com.example.mem;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
//import android.support.v7.app.*;
//import android.support.v7.view.

public class MemInfo extends FragmentActivity {
    ViewPager viewPager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //final ActionBar actionBar = getSupportActionBar();
        setContentView(R.layout.activity_main);


        viewPager = (ViewPager) findViewById(R.id.pager);
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        viewPager.setAdapter(new Adapter(fragmentManager));
    }


    class Adapter extends FragmentPagerAdapter {

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new UsageDataFragment();
                case 1:
                    return new ProcessInfoFragment();
                case 2:
                    return new SystemInfoFragment();
                default:
                    return null;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Usage";
                case 1:
                    return "Processes";
                case 2:
                    return "System";
                default:
                    return "Null tab";
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
