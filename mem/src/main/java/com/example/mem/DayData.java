package com.example.mem;

import android.os.Environment;
import android.os.StatFs;

import java.util.Calendar;

/**
 * Contains all the data the database contains for a single day
 */
public class DayData {
    private String date;
    private String internalMemory;
    private String externalMemory;

    public DayData(boolean New) {
        if (New) {
            this.getTodaysData();
        }
    }

    private void getTodaysData() {
        this.internalMemory = getStorage(Environment.getDataDirectory().getAbsolutePath());

        this.externalMemory = getStorage(Environment.getExternalStorageDirectory().getAbsolutePath());

        Calendar calendar_instance = Calendar.getInstance();
        this.date = "" + calendar_instance.get(Calendar.YEAR) + "/" +
                (calendar_instance.get(Calendar.MONTH) + 1) + "/" +
                calendar_instance.get(Calendar.DAY_OF_MONTH);
    }

    public String toString() {
        return "Date: " + this.date + "\nInternal Memory: " + this.internalMemory + "\nExternal Memory: "
                + this.externalMemory + "\n------------------------------------------------------\n";
    }

    public String getStorage(String path) {
        StatFs stat = new StatFs(path);
        long blockSize = stat.getBlockSize();
        long availBlocks = stat.getAvailableBlocks();
        float free_memory = (long) availBlocks * (long) blockSize;
        return FormatUtility.storageFormat(free_memory);
    }

    public void setNewData(String date, String Internal, String External) {
        this.date = date;
        this.internalMemory = Internal;
        this.externalMemory = External;
    }

    public String getInternalMemory() {
        return this.internalMemory;
    }

    public String getExternalMemory() {
        return this.externalMemory;
    }

    public String getDate() {
        return this.date;
    }
}
