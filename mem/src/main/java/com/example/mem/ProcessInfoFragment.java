package com.example.mem;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.List;

import static android.app.ActivityManager.RunningAppProcessInfo;

/**
 * Process info tab
 */
public class ProcessInfoFragment extends Fragment {
    View view;
    ExpandableListView listView = null;
    Context activity = null;
    ProcessAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.process_info_tab, container, false);
        listView = (ExpandableListView) view.findViewById(R.id.processList);
        activity = this.getActivity();

        ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);

        List<RunningAppProcessInfo> appProcessInfoList = am.getRunningAppProcesses();

        adapter = new ProcessAdapter(activity,
                R.layout.process_view_item,
                R.layout.process_view_item_child, appProcessInfoList);

        listView.setAdapter(adapter);
        //*/
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //adapter
    }

    // Inner class, adapter for the expandable list view
    class ProcessAdapter extends BaseExpandableListAdapter {
        Context context;
        int groupResourceId;
        int childResourceId;
        List<RunningAppProcessInfo> processData;
        //Handler parentTimerHandler, childTimerHandler;
        //Runnable updateParentDisplay, updateChildDisplay;

        public ProcessAdapter(Context context, int groupLayoutId, int childLayoutId,
                              List<RunningAppProcessInfo> objects) {

            this.context = context;
            this.groupResourceId = groupLayoutId;
            this.childResourceId = childLayoutId;
            this.processData = objects;
        }

        @Override
        public View getGroupView(int position, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            View row = convertView;
            ProcessInfoHolder holder = null;

            // If row is not null it means that the views have been initialized and we can simply
            // retrieve them from the parent view via the tag field in which they are saved after
            // initialization.
            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(this.groupResourceId, parent, false);

                holder = new ProcessInfoHolder();
                holder.processName = (TextView) row.findViewById(R.id.processName);
                holder.pid = (TextView) row.findViewById(R.id.processId);
                holder.mem = (TextView) row.findViewById(R.id.processMem);

                row.setTag(holder);
            } else {
                // Saving some compute time by utilizing the tag field of the View class
                holder = (ProcessInfoHolder) row.getTag();
            }

            RunningAppProcessInfo process = this.processData.get(position);
            Debug.MemoryInfo memInfo = (Debug.MemoryInfo) this.getChild(position, 0);

            updateGroupView(holder, process, memInfo);

            return row;
        }

        private void updateGroupView(ProcessInfoHolder holder,
                                     RunningAppProcessInfo process,
                                     Debug.MemoryInfo memInfo) {

            holder.processName.setText(process.processName);
            holder.pid.setText(Integer.toString(process.pid));
            holder.mem.setText(FormatUtility.memFormat(memInfo.getTotalPss()));
        }

        @Override
        public View getChildView(int i, int i2, boolean b, View view, ViewGroup viewGroup) {
            Debug.MemoryInfo childMemInfo;
            View row = view;
            DetailedMemoryInfoHolder holder = null;

            // If row is not null it means that the views have been initialized and we can simply
            // retrieve them from the parent view via the tag field in which they are saved after
            // initialization.
            if (row == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                row = inflater.inflate(this.childResourceId, viewGroup, false);

                holder = new DetailedMemoryInfoHolder();
                holder.dalvikPrivate = (TextView) row.findViewById(R.id.mem_dalvik_private);
                holder.dalvikShared = (TextView) row.findViewById(R.id.mem_dalvik_shared);
                holder.dalvikPSS = (TextView) row.findViewById(R.id.mem_dalvik_pss);
                holder.nativePrivate = (TextView) row.findViewById(R.id.mem_native_private);
                holder.nativeShared = (TextView) row.findViewById(R.id.mem_native_shared);
                holder.nativePSS = (TextView) row.findViewById(R.id.mem_native_pss);
                holder.otherPrivate = (TextView) row.findViewById(R.id.mem_other_private);
                holder.otherShared = (TextView) row.findViewById(R.id.mem_other_shared);
                holder.otherPSS = (TextView) row.findViewById(R.id.mem_other_pss);
                row.setTag(holder);
            } else {
                // Saving some compute time by utilizing the tag field of the View class
                holder = (DetailedMemoryInfoHolder) row.getTag();
            }

            childMemInfo = (Debug.MemoryInfo) this.getChild(i, i2);
            updateChildView(holder, childMemInfo);

            return row;
        }

        private void updateChildView(DetailedMemoryInfoHolder holder, Debug.MemoryInfo memInfo) {

            holder.dalvikPrivate.setText(FormatUtility.memFormat(memInfo.dalvikPrivateDirty));
            holder.dalvikShared.setText(FormatUtility.memFormat(memInfo.dalvikSharedDirty));
            holder.dalvikPSS.setText(FormatUtility.memFormat(memInfo.dalvikPss));
            holder.nativePrivate.setText(FormatUtility.memFormat(memInfo.nativePrivateDirty));
            holder.nativeShared.setText(FormatUtility.memFormat(memInfo.nativeSharedDirty));
            holder.nativePSS.setText(FormatUtility.memFormat(memInfo.nativePss));
            holder.otherPrivate.setText(FormatUtility.memFormat(memInfo.otherPrivateDirty));
            holder.otherShared.setText(FormatUtility.memFormat(memInfo.otherSharedDirty));
            holder.otherPSS.setText(FormatUtility.memFormat(memInfo.otherPss));
        }


        @Override
        public int getGroupCount() {
            return this.processData.size();
        }

        @Override
        public int getChildrenCount(int i) {
            // A linear layout that can contain many elements.
            return 1;
        }

        @Override
        public Object getGroup(int i) {
            return this.processData.get(i);
        }

        @Override
        public Object getChild(int i, int i2) {
            RunningAppProcessInfo process = this.processData.get(i);
            int pid[] = {process.pid};
            ActivityManager am = (ActivityManager)
                    activity.getSystemService(Context.ACTIVITY_SERVICE);

            // Debug doesn't accept single elements due to it being optimized for speed
            // and not for a convinient API as explained in the documents so we simply create a
            // single element array to pass it and retrieve the first and only element of the
            // returning list.
            Debug.MemoryInfo memoryInfo = am.getProcessMemoryInfo(pid)[0];

            // Unnecessary check
            if (i2 == 0) {
                return memoryInfo;
            } else {
                return null;
            }
        }

        // It's one instance of a layout which can contain an arbitrary number of elements.
        @Override
        public long getGroupId(int i) {
            return i;
        }

        @Override
        public long getChildId(int i, int i2) {
            return i2;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int i, int i2) {
            return false;
        }
    }

    /**
     * Static helper class which allows the list item-group views to be saved
     * in the tag field of the View class
     */
    private static class ProcessInfoHolder {
        TextView processName;
        TextView pid;
        TextView mem;
    }

    /**
     * Static helper class which allows the list item detail views
     * to be saved in the tag field of the View class
     */
    private static class DetailedMemoryInfoHolder {
        TextView dalvikPrivate;
        TextView dalvikShared;
        TextView dalvikPSS;
        TextView nativePrivate;
        TextView nativeShared;
        TextView nativePSS;
        TextView otherPrivate;
        TextView otherShared;
        TextView otherPSS;
    }
}

