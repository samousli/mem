package com.example.mem;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SystemInfoFragment extends Fragment {
    View view;
    Handler timerhandler;
    Runnable updateDisplay;

    public SystemInfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        timerhandler = new Handler();
        this.view = inflater.inflate(R.layout.system_info_tab, container, false);
        // Update view every second
        updateDisplay = new Runnable() {
            @Override
            public void run() {
                Display();
                timerhandler.postDelayed(this, 1000);
            }
        };
        timerhandler.post(updateDisplay);
        return view;
    }

    @Override
    /**
     * On view destruction removing all the queued callbacks
     * to the delayed UI updates. Otherwise the runnable may try to
     * display the destroyed object.
     * P.S. This was the cause of the crashes.
     */
    public void onDestroyView() {
        super.onPause();
        timerhandler.removeCallbacks(updateDisplay);
    }

    public void Display() {
        String line;

        TextView name = (TextView) view.findViewById(R.id.name_id);
        line = this.getDeviceName() + "\n";
        name.setText(line);

        TextView battery = (TextView) view.findViewById(R.id.battery_id);
        line = this.batteryLevel() + "%\n";
        battery.setText(line);

        TextView kernel = (TextView) view.findViewById(R.id.kernel_id);
        line = this.getKernelVersion() + "\n";
        kernel.setText(line);

        TextView android = (TextView) view.findViewById(R.id.android_id);
        line = this.getAndroidVersion() + "\n";
        android.setText(line);

        TextView uptime = (TextView) view.findViewById(R.id.uptime_id);
        line = this.getUptime() + "\n";
        uptime.setText(line);

        TextView storage = (TextView) view.findViewById(R.id.storage_id);
        line = this.getStorage(Environment.getDataDirectory().getAbsolutePath()) + "\n";
        storage.setText(line);

        TextView external_storage = (TextView) view.findViewById(R.id.external_storage_id);
        line = this.getStorage(Environment.getExternalStorageDirectory().getAbsolutePath()) + "\n";
        external_storage.setText(line);

        TextView connection = (TextView) view.findViewById(R.id.connection_id);
        line = this.getConnectivity();
        if (line == null) {
            line = "No connection";
        }
        connection.setText(line + "\n");
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        } else {
            return manufacturer + " " + model;
        }
    }

    public String batteryLevel() {
        Intent intent = getActivity().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        return Integer.toString(level);
    }

    public String getKernelVersion() {
        String kernel = System.getProperty("os.version");
        return kernel;
    }

    public String getAndroidVersion() {
        String version = Build.DISPLAY;
        return version;
    }

    public String getUptime() {
        long uptime = SystemClock.elapsedRealtime();
        int seconds = (int) (uptime / 1000) % 60;
        int minutes = (int) ((uptime / (1000 * 60)) % 60);
        int hours = (int) ((uptime / (1000 * 60 * 60)) % 24);
        return hours + "hrs." + minutes + "min." + seconds + "sec.";
    }

    public String getStorage(String path) {
        StatFs stat = new StatFs(path);
        long blockSize = stat.getBlockSize();
        long availBlocks = stat.getAvailableBlocks();
        float free_memory = (long) availBlocks * (long) blockSize;
        return FormatUtility.storageFormat(free_memory);
    }

    public String getConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
        if (isConnected) {
            int connection = activeNetwork.getType();
            if (connection == 0) {
                return "Mobile network";
            } else if (connection == 1) {
                return "WiFi network";
            } else if (connection == 2) {
                return "WiMax connection";
            } else if (connection == 3) {
                return "Ethernet connection";
            } else if (connection == 4) {
                return "Bluetooth connection";
            }
            return Integer.toString(activeNetwork.getType());
        } else {
            return null;
        }
    }
}

